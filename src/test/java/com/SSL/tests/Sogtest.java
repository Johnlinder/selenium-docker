package com.SSL.tests;


import com.SSL.pages.SSLFrontPage;
import com.SSL.pages.SSLscanResultspage;
import com.searchmodule.pages.SearchPage;
import com.tests.BaseTest;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.Map;

import static org.testng.Assert.assertTrue;

// @Epic("Security tests Sogeti homepage")
public class Sogtest extends BaseTest {




    @Test
    public void searchSogeti(){
        SSLFrontPage frontpage = new SSLFrontPage(driver);
        SSLscanResultspage results = new SSLscanResultspage(driver);
        frontpage.goTo();
        assertTrue(results.doSearchSogeti());
    }





}

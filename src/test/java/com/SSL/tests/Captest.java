package com.SSL.tests;


import com.SSL.pages.SSLFrontPage;
import com.SSL.pages.SSLscanResultspage;
import com.tests.BaseTest;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

// @Epic("Security tests Sogeti homepage")
public class Captest extends BaseTest {



    @Test
    public void searchCapgemini(){
        SSLFrontPage frontpage = new SSLFrontPage(driver);
        SSLscanResultspage results = new SSLscanResultspage(driver);
        frontpage.goTo();
        assertTrue(results.doSearchCapgemini());
    }



}

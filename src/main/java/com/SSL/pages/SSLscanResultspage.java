package com.SSL.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Map;

public class SSLscanResultspage {




    private WebDriver driver;
    private WebDriverWait wait;

    public SSLscanResultspage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 300);
        PageFactory.initElements(driver, this);
    }



    //public By searchBtn() {return By.cssSelector("#main > div.submitBox > center > form > table > tbody > tr:nth-child(1) > td:nth-child(3) > input[type=submit]");}

    @FindBy(css="#main > div.submitBox > center > form > table > tbody > tr:nth-child(1) > td:nth-child(3) > input[type=submit]")
    private WebElement searchBtn;

    @FindBy(css="#main > div.submitBox > center > form > table > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=text]")
    private WebElement searchBx;

    @FindBy(xpath="//*[@id=\"main\"]/div[2]/a")
    private WebElement clearCache;

    @FindBy(xpath= "//*[@id=\"gradeA\"]")
    private WebElement SogetiGrade;

    @FindBy(xpath= "//*[@id=\"multiTable\"]/tbody/tr[3]/td[4]/div")
    private WebElement CapgeminiGrade;
    //public By searchBx() {return By.xpath("//*[@id=\"main\"]/div[1]/center/form/table/tbody/tr[1]/td[2]/input");}

    //public By searchBx() { return By.cssSelector("#main > div.submitBox > center > form > table > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=text]"); }

    //public By clearCache() { return  By.xpath("//*[@id=\"main\"]/div[2]/a"); }
    // public By clearCache() { return  By.cssSelector( "#main > div.reportTime.floatLeft > a:nth-child(3)"); }


    public boolean doSearchSogeti(){
        String grade = "nada";
        this.wait.until(ExpectedConditions.visibilityOf(this.searchBtn));
        this.searchBx.sendKeys("sogeti.se");
        this.searchBtn.click();
        this.wait.until(ExpectedConditions.visibilityOf(this.clearCache));
        this.clearCache.click();

        this.wait.until(ExpectedConditions.visibilityOf(this.SogetiGrade));
        grade = this.SogetiGrade.getText();
        System.out.println(grade);
        String existingGrade = "A";
        if(grade.equals(existingGrade)){
            return true;}
        else {

            return false;
        }
    }


    public boolean doSearchCapgemini(){
        String grade = "nada";
        this.wait.until(ExpectedConditions.visibilityOf(this.searchBtn));
        this.searchBx.sendKeys("capgemini.com");
        this.searchBtn.click();
        this.wait.until(ExpectedConditions.visibilityOf(this.clearCache));
        this.clearCache.click();
        this.wait.until(ExpectedConditions.visibilityOf(this.CapgeminiGrade));
        grade = this.CapgeminiGrade.getText();
        System.out.println(grade);
        String existingGrade = "A+";
        if(grade.equals(existingGrade)){
            return true;}
        else {

            return false;
        }
                }}






   /* public By mobileTopNavBtn() {
        return By.cssSelector(".navbar-toggle");
    }



    public By topmenuUL() {
        switch (driverClass().toUpperCase()) {
            case "ANDROIDDRIVER":
                return By.cssSelector(".main-menu > ul:nth-child(1)");
            default:
                return By.cssSelector(".main-menu-desktop > ul:nth-child(1)");
        }
    }

    public By aboutUsMenuUL() {
        switch (driverClass().toUpperCase()) {
            case "ANDROIDDRIVER":
                return By.cssSelector("#header > nav > ul > li.has-children.level0.expanded > div > div > ul");
            default:
                return By.cssSelector("div.mega-navbar:nth-child(4) > ul:nth-child(2)");
        }
    }

    public By mobileSearchFieldBtn() {
        return By.cssSelector(".search-button");
    }

    public WebElement topLogo() {
        return driver().findElement(By.cssSelector(".current"));
    }*/


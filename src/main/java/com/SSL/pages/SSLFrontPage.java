package com.SSL.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SSLFrontPage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(css="#page > div.mainPhoto > div > a:nth-child(1)")
    private WebElement testYourServer;

    public SSLFrontPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    public void goTo(){
        this.driver.get("https://www.ssllabs.com/");
        this.wait.until(ExpectedConditions.visibilityOf(this.testYourServer));
        this.testYourServer.click();
    }

}


package com.Sogeti.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SogetiPage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(name="q")
    private WebElement searchTxt;

    @FindBy(css = "#header > div.desktop_wrapper > nav > ul > li.has-children.level1 > div.mega-navbar.refreshed.level1 > ul > li:nth-child(4) > a")
    private WebElement deliveryCenter;

    @FindBy(css = "#header > div.desktop_wrapper > nav > ul > li.has-children.level1 > div.wrapper > span")
    private WebElement whySogeti;

    @FindBy(css = "#CookieConsent > div.consentcontent > div > div.flexcontainer > div.buttons > button.acceptCookie")
    private WebElement cookieConsent;

    public SogetiPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    public void goTo(){
        this.driver.get("https://sogeti.se/");
        this.wait.until(ExpectedConditions.visibilityOf(this.cookieConsent));
        this.cookieConsent.click();
        this.wait.until(ExpectedConditions.visibilityOf(this.whySogeti));
        this.whySogeti.click();
        this.wait.until(ExpectedConditions.visibilityOf(this.deliveryCenter));
        this.deliveryCenter.click();
    }










}

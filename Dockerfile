FROM openjdk:8u191-jre-alpine3.8

RUN apk add curl jq

# Workspace
WORKDIR /Users/jlinder/IdeaProjects/selenium-docker

# ADD .jar under target from host
# into this image
ADD target/selenium-docker.jar 			selenium-docker.jar
ADD target/selenium-docker-tests.jar 	selenium-docker-tests.jar
ADD target/libs							libs


# in case of any other dependency like .csv / .json / .xls
# please ADD that as well

# ADD suite files
ADD SSLScan.xml			                SSLScan.xml
ADD search-module.xml					search-module.xml
ADD SogetiTest.xml                      SogetiTest.xml
# ADD health check script
ADD healthcheck.sh                      healthcheck.sh
RUN dos2unix healthcheck.sh

# BROWSER
# HUB_HOST
# MODULE

ENTRYPOINT sh healthcheck.sh